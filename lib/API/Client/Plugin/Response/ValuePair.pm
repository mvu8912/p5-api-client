package API::Client::Plugin::Response::ValuePair;

use strict;
use warnings;
use Mouse;

with "API::Client::Role::Plugin";

no Mouse;

sub value_pair_response {
    my $api = shift->api;
    my @pairs = split /&/, $api->last_response->decoded_content || q{};
    my $data =
      { map { my ( $k, $v ) = split /=/, $_, 2; ( $k => $v ) } @pairs };

    if ( my $error = "$@" ) {
        $data = { status => "error", error => $error };
    }
    return $data;
}

1;

package API::Client::Plugin::Response::JSON;

use strict;
use warnings;
use JSON::XS;
use Mouse;
use Try::Tiny;

with "API::Client::Role::Plugin";

has json => (
    is         => "rw",
    isa        => "JSON::XS",
    lazy_build => 1,
);

sub _build_json { JSON::XS->new->utf8 }

no Mouse;

sub json_response {
    my $api      = shift->api;
    my $response = undef;
    try {
        my $last_response = $api->last_response->decoded_content;
        $response = $api->json->decode( $last_response || "{}" );
    }
    catch {
        my $error = $_;
        $response = { status => "error", error => $error };
    };
    return $response;
}

1;

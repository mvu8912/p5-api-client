package API::Client::Plugin::Response::Raw;

use strict;
use warnings;
use Mouse;

with "API::Client::Role::Plugin";

no Mouse;

sub raw_response_content {
    my $api = shift->api;
    return $api->last_response->decoded_content;
}

sub raw_response_transaction {
    my $api  = shift->api;
    my $resp = $api->last_response;
    my $req  = $resp->request;
    return
        "------------\n"
      . ">> REQUEST: \n"
      . "------------\n"
      . $req->as_string
      . "------------\n"
      . ">> RESPONSE: \n"
      . "------------\n"
      . $resp->as_string
      . "------------\n";
}

1;

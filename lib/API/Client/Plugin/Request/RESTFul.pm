package API::Client::Plugin::Request::RESTFul;

use strict;
use warnings;
use Mouse;

with "API::Client::Role::RequestPlugin";

has json => (
    is         => "rw",
    isa        => "JSON::XS",
    lazy_build => 1,
);

sub _build_json { JSON::XS->new->canonical(1)->utf8 }

no Mouse;

sub content_type_converter {
    return (
        "application/json" => "hash_to_json",
        "text/json"        => "hash_to_json",
    );
}

sub hash_to_json {
    my $self = shift;
    my %data = @_;
    return $self->json->encode( \%data );
}

1;

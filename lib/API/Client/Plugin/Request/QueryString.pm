package API::Client::Plugin::Request::ValuePair;

use strict;
use warnings;
use URI;
use Mouse;

with "API::Client::Role::RequestPlugin";

no Mouse;

sub content_type_converter {
    return ( "application/x-www-form-urlencoded" => "hash_to_ValuePair" );
}


sub hash_to_ValuePair {
    my $self = shift;
    my %hash = @_;
    my $uri  = URI->new("http://parser.com");
    $uri->query_form( \%hash );
    my ( undef, $params ) = split /\?/, $uri->as_string;
    return $params;
}

1;

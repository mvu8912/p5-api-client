package API::Client::Plugin::Auth::BasicAuth;

use strict;
use warnings;
use Mouse;

with "API::Client::Role::Plugin";

no Mouse;

sub basic_auth {
    my $self     = shift;
    my %args     = @_;
    my $request  = $args{request};
    my $username = $args{username};
    my $password = $args{password};
    $request->headers->authorization_basic( $username, $password );
}

sub auth_token {
    my $self    = shift;
    my %args    = @_;
    my $headers = $args{headers_in_hash};
    $headers->{authorization} = $args{token};
}

1;

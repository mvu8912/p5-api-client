package API::Client::Role::RequestPlugin;

use strict;
use warnings;
use Mouse::Role;

with "API::Client::Role::Plugin";

requires "content_type_converter";

no Mouse::Role;

1;

package API::Client::Role::Debug;

use strict;
use warnings;
use Mouse::Role;

my @FLAGS = qw(
  DEBUG_SEND_OUT
  DEBUG_IN_OUT
  DEBUG_RESPONSE
  DEBUG_RESPONSE_IF_FAIL
  DEBUG_RESPONSE_HEADER_ONLY
  RETRY_FAIL_RESPONSE
  RETRY_FAIL_STATUS
  RETRY_DELAY
);

foreach my $flag (@FLAGS) {
    has $flag => (
        is      => "rw",
        isa     => "Str",
        lazy    => 1,
        default => sub { $ENV{$flag} // 0 }
    );
}

no Mouse::Role;

1;

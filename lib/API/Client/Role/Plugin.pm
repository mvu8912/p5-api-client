package API::Client::Role::Plugin;

use strict;
use warnings;
use Mouse::Role;

has api => (
    is       => "rw",
    isa      => "API::Client",
    required => 1,
);

no Mouse::Role;

1;

package API::Client;

use strict;
use warnings;

=head1 NAME

API::Client - API Client

=head1 USAGE

 use API::Client;

 my $ua1 = API::Client->new;
 my $ua2 = API::Client->new(base_url => URI->new( $url ) );
 my $ua3 = API::Client->new(base_url => URI->new( $url ) );

 $ua->send( $method, $url, \%data, \%header );

Send short hand methods - get, post, head, put and delete

Example:

 $ua->get( $url ) same as $ua->send( GET, $url );
 $ua->post( $url, \%data, \%headers ) same as $ua->send( GET, $url, \%data, \%headers );

Get Json Data - grab the content body from the response and json decode

 my $data = $ua->json_response;

At the moment, only support query string and json data in and out

=cut

use HTTP::Headers;
use HTTP::Request;
use LWP::UserAgent;
use Mouse;
use Try::Tiny;
use Module::Pluggable (
    require     => 1,
    sub_name    => "_plugins",
    instantiate => "new"
);

with "API::Client::Role::Debug";

has base_url => (
    is  => "rw",
    isa => "URI",
);

has last_response => (
    is  => "rw",
    isa => "HTTP::Response",
);

has charset => (
    is      => "rw",
    isa     => "Str",
    default => "utf8",
);

has browser_id => (
    is         => "rw",
    isa        => "Str",
    lazy_build => 1,
);

sub _build_browser_id {
    my $self = shift;
    my $ver = $API::Client::VERSION || -1;
    return "Monitoring API Client v$ver";
}

has content_type => (
    is         => "rw",
    isa        => "Str",
    lazy_build => 1,
);

sub _build_content_type {
    my $self    = shift;
    my $charset = $self->charset;
    return "application/json; charset=$charset";
}

has ua => (
    is         => "rw",
    isa        => "LWP::UserAgent",
    lazy_build => 1,
);

sub _build_ua {
    my $self       = shift;
    my $ssl_verify = $self->ssl_verify;
    my $ua =
      LWP::UserAgent->new( ssl_opts => { verify_hostname => $ssl_verify } );
    $ua->agent( $self->browser_id );
    $ua->timeout( $self->timeout );
    return $ua;
}

sub _build_retry {
    my $self = shift;
    my $count = $self->RETRY_FAIL_RESPONSE // 0;
    my $status =
      $self->RETRY_FAIL_STATUS
      ? eval { $self->RETRY_FAIL_STATUS }
      : {};

    if ( my $error = $@ ) {
        die "RETRY_FAIL_STATUS error: $error";
    }

    my $delay = $self->RETRY_DELAY // 5;

    return {
        count  => $count,
        status => $status,
        delay  => $delay,
    };
}

has ssl_verify => (
    is         => "rw",
    isa        => "Bool",
    lazy_build => 1,
);

sub _build_ssl_verify { $ENV{SSL_VERIFY} // 0 }

has retry => (
    is         => "rw",
    isa        => "HashRef",
    lazy_build => 1,
);

has timeout => (
    is      => "rw",
    isa     => "Int",
    default => 60,
);

has need_auth => (
    is         => "rw",
    isa        => "Bool",
    lazy_build => 1,
);

sub _build_need_auth {
    my $self = shift;
    return ( $self->username && $self->password ) ? 1 : 0;
}

has auth_method => (
    is      => "rw",
    isa     => "Str",
    default => "basic_auth",
);

has auth_args => (
    is         => "rw",
    isa        => "HashRef",
    auto_deref => 1,
    lazy_build => 1,
);

sub _build_auth_args {
    my $self = shift;
    return {
        username => $self->username,
        password => $self->password,
    };
}

has username => (
    is  => "rw",
    isa => "Str",
);

has password => (
    is  => "rw",
    isa => "Str",
);

has _content_converter_mapping => (
    is         => "ro",
    isa        => "HashRef",
    traits     => ["Hash"],
    lazy_build => 1,
    handles    => {
        set_content_converter => "set",
        get_content_converter => "get",
    },
);

sub _build__content_converter_mapping { {} }

has plugins => (
    is         => "ro",
    isa        => "HashRef",
    lazy_build => 1,
);

sub _build_plugins {
    my $self = shift;
    my %plugins = ();
    foreach my $plugin_object( $self->_plugins( api => $self ) ) {
        my $class = ref $plugin_object;
        $class =~s/^API::Client::Plugin:://;
        $plugins{$class} = $plugin_object;
    }
    return \%plugins;
}

no Mouse;

sub get {
    my $self = shift;
    return $self->send( GET => @_ );
}

sub post {
    my $self = shift;
    return $self->send( POST => @_ );
}

sub put {
    my $self = shift;
    return $self->send( PUT => @_ );
}

sub head {
    my $self = shift;
    return $self->send( HEAD => @_ );
}

sub delete {
    my $self = shift;
    return $self->send( DELETE => @_ );
}

sub send {
    my $self         = shift;
    my $method       = shift || "GET";
    my $path         = shift;
    my $data         = shift || {};
    my $headers      = shift || {};
    my $ua           = $self->ua;
    my $base_url     = $self->base_url;
    my $url          = $base_url ? $base_url . $path : $path;
    my $req          = $self->_request( $method, $url, $data, $headers );
    my $retry_count  = $self->retry->{count} // 1;
    my %retry_status = %{ $self->retry->{status} || {} };
    my $retry_delay  = $self->retry->{delay} // 5;

    my $response;

  RETRY:
    foreach my $retry ( 0 .. $retry_count ) {
        my $started_time = time;

        $response = $ua->request($req);

        if ( $self->DEBUG_IN_OUT || $self->DEBUG_SEND_OUT ) {
            print STDERR "-- REQUEST --\n";
            if ( $retry_count && $retry ) {
                print STDERR "-- RETRY $retry of $retry_count\n";
            }
            print STDERR $response->request->as_string;
            print STDERR "\n";
        }

        my $debug_response = $self->DEBUG_IN_OUT || $self->DEBUG_RESPONSE;

        $debug_response = 0
          if $self->DEBUG_RESPONSE_IF_FAIL && $response->is_success;

        if ($debug_response) {
            my $used_time = time - $started_time;

            print STDERR "-- RESPONSE $used_time sec(s) --\n";

            print STDERR $self->DEBUG_RESPONSE_HEADER_ONLY
              ? $response->headers->as_string
              : $response->as_string;

            print STDERR ( "-" x 80 ) . "\n";
        }

        last RETRY    ## request is success, not further for retry
          if $response->is_success;

        if ( !%retry_status ) {
            sleep $retry_delay;
            ## no retry pattern at all then just retry
            next RETRY;
        }

        my $pattern = $retry_status{ $response->code }
          or
          last RETRY;  ## no retry pattern for this status code, just stop retry

        ## retry if pattern is match otherwise, just stop retry
        if ( $response->decode_content =~ /$pattern/ ) {
            sleep $retry_delay;
            next RETRY;
        }

        last RETRY;
    }

    return $self->last_response($response);
}

## Load methods from plugins
our $AUTOLOAD;

sub AUTOLOAD {
    my $self   = shift;
    my @args   = @_;
    my $method = $AUTOLOAD;
    $method =~ s/.*:://;

    if ( my $content_type = _is_try_convert_content_for_request($method) ) {
        $method = $self->_get_converter_method_from_content_type($content_type);
    }

    my $plugins_ref = $self->plugins;

    foreach my $plugin_name ( sort keys %$plugins_ref ) {
        my $plugin_object = $plugins_ref->{$plugin_name};
        if ( $plugin_object->can($method) ) {
            return $plugin_object->$method(@args);
        }
    }

    die "Invalid method $method.\n";
}

sub _request {
    my $self    = shift;
    my $method  = uc shift;
    my $url     = shift;
    my $data    = shift;
    my $headers = shift || {};

    my $create_req = sub {
        my $uri = shift;
        my $req = HTTP::Request->new( $method => $uri );

        $req->content_type( $self->content_type )
          if $method !~ /get/i;

        if ( $self->need_auth ) {
            my $method = $self->auth_method;
            $self->$method(
                request         => $req,
                headers_in_hash => $headers,
                $self->auth_args,
            );
        }

        return $req;
    };

    my $req = $create_req->($url);

    my $content = _tune_utf8( $self->_convert_data( $req, $data ) );

    if ( $method =~ /get/i ) {
        $req = $create_req->( $content ? "$url?$content" : $url );
    }

    foreach my $field ( keys %$headers ) {
        $req->header( $field => $headers->{$field} );
    }

    if ( $method !~ /get/i ) {
        $req->content($content);
    }

    return $req;
}

sub _tune_utf8 {
    my $content = shift;
    my $req = HTTP::Request->new( POST => "http://find-encoding.com" );
    try {
        $req->content($content);
    }
    catch {
        my $error = $_;
        if ( $error =~ /content must be bytes/ ) {
            eval { $content = Encode::encode( utf8 => $content ); };
        }
    };
    return $content;
}

sub _convert_data {
    my $self = shift;
    my $req  = shift;
    my $data = shift
      or return;

    return $data
      if !ref $data;

    my $content_type =
      $req->content_type || "application/x-www-form-urlencoded";

    my $converter_method = "CONVERT-CONTENT-$content_type";

    return $self->$converter_method(%$data);
}

sub _is_try_convert_content_for_request {
    my $string = shift;
    my ($content_type) = ( $string =~ /^CONVERT-CONTENT-(.+)/ );
    return $content_type;
}

{
    my %TRIED_BEFORE_CONVERTER = ();

    sub _get_converter_method_from_content_type {
        my $self         = shift;
        my $content_type = shift
          or die "FIXME: Missing content type.";
        my $method = $self->get_content_converter($content_type);

        return $method
          if $method;

        my $error =
          "FIXME: Content-Type: $content_type doesn't have converter for request";

        die $error
          if $TRIED_BEFORE_CONVERTER{$content_type};

        $TRIED_BEFORE_CONVERTER{$content_type} = 1;

        foreach my $plugin ( $self->_plugins( api => $self ) ) {
            my $importer = $plugin->can("content_type_converter")
              or next;
            $self->set_content_converter( $importer->$importer );
        }

        $method = $self->get_content_converter($content_type)
          or die $error;

        return $method;
    }
}

1;

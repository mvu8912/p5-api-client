use strict;
use warnings;

use API::Client;
use HTTP::Daemon;
use HTTP::Response;
use Test::More;
use Test::TCP;
use URI;

Test::TCP::test_tcp(
    client => sub {
        my $port = shift;
        my $api  = API::Client->new(
            base_url     => URI->new("http://127.0.0.1:$port"),
            content_type => "application/x-www-form-urlencoded",
        );
        $api->get("/OK");
        is $api->raw_response_content, "OK";
        $api->post( "/HERE" => { data => "foobar" } );
        is $api->raw_response_content, "HERE-data=foobar";
    },
    server => sub {
        my $tester = HTTP::Daemon->new( LocalPort => shift );
        while ( my $conn = $tester->accept ) {
            while ( my $req = $conn->get_request ) {
                my @content = ();
                if ( $req->uri->path =~ /\/(.+)/ ) {
                    push @content, $1;
                }
                if ( $req->method eq "POST" ) {
                    is $req->content_type,
                      qq{application/x-www-form-urlencoded};
                    push @content, $req->content;
                }
                my $res = HTTP::Response->new(200);
                $res->content( join "-", @content );
                $conn->send_response($res);
            }
            $conn->close;
        }
    }
);

done_testing;
